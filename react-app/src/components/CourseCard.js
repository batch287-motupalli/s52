import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState } from 'react';

export default function CourseCard({course}) {

const { name, description, price } = course;

// console.log(course.name);
// console.log(course.price);
// console.log(course.description);

// React Hook that lets you add a state variable to the components.
// Use the state hook for this component to be able to store its state
// States are used to keep track of information related to individual components

// Syntax:
	// const [ getter, setter ] = useState(initialGetterValue)

// State Hook
const [ count, setCount ] = useState(0);
const [ seats, setSeats ] = useState(30);

function enroll() {
	if(seats > 0){
		setCount(count + 1);
		console.log('Enrollees: ' + count)
		setSeats(seats - 1);
		console.log('Seats: ' + seats)
	} else {
		alert("No more seats available!");
	};
};


	return (
		<Row className="mt-3 mb-3">
			<Col xs={12}>
				<Card className="cardHighlight p-0">
					<Card.Body>
						<Card.Title><h4>{name}</h4></Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Card.Subtitle>Enrollees:</Card.Subtitle>
						<Card.Text>{count} Enrollees</Card.Text>
						<Button variant="primary" onClick={enroll}>Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
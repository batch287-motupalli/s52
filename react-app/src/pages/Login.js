import { Form, Button, h1 } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Register() {

	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {

		if(email !== "" && password1 !== "" ){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	})

	function registerUser(e) {
		e.preventDefault();

			setEmail("");
			setPassword1("");

		alert("Successful login!");
	};
	
	return (
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>
			<br></br>

			{ 
				isActive ? 
				<Button variant="success" type="submit" id="submitBtn">
					Login
				</Button>
				:
				<Button variant="success" type="submit" id="submitBtn" disabled>
					Login
				</Button>

			}

		</Form>
	)
}